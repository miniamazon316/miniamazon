﻿
/* INSERT QUERY NO: 1 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
1, 1, ' 8/20/2002', ' 5/28/2007', 'Pending', 29.97
);

/* INSERT QUERY NO: 2 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
2, 2, ' 3/12/2004', ' 11/11/2007', 'Delivered', 74.95
);

/* INSERT QUERY NO: 3 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
3, 3, ' 1/10/2009', ' 11/17/2019', 'Shipped', 9.99
);

/* INSERT QUERY NO: 4 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
4, 4, ' 11/14/2000', ' 2/17/2008', 'Delivered', 11.99
);

/* INSERT QUERY NO: 5 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
5, 5, ' 7/5/2001', ' 4/8/2007', 'Delivered', 11.98
);

/* INSERT QUERY NO: 6 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
6, 6, ' 11/2/2011', ' 1/1/2013', 'Delivered', 59.96
);

/* INSERT QUERY NO: 7 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
7, 7, ' 5/11/2019', ' 8/9/2019', 'Shipped', 50.4
);

/* INSERT QUERY NO: 8 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
8, 8, ' 3/13/2007', ' 3/14/2013', 'Delivered', 14.97
);

/* INSERT QUERY NO: 9 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
9, 9, ' 4/13/2009', ' 11/16/2015', 'Delivered', 27.96
);

/* INSERT QUERY NO: 10 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
10, 10, ' 1/23/2010', ' 4/20/2011', 'Shipped', 174.95
);

/* INSERT QUERY NO: 11 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
11, 11, ' 12/17/2000', ' 7/8/2019', 'Delivered', 59.96
);

/* INSERT QUERY NO: 12 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
12, 12, ' 7/1/2006', ' 1/29/2007', 'Shipped', 70
);

/* INSERT QUERY NO: 13 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
13, 13, ' 4/1/2017', ' 7/15/2017', 'Delivered', 43.96
);

/* INSERT QUERY NO: 14 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
14, 14, ' 5/1/2003', ' 4/23/2012', 'Pending', 17.98
);

/* INSERT QUERY NO: 15 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
15, 15, ' 8/12/2002', ' 12/26/2011', 'Delivered', 48
);

/* INSERT QUERY NO: 16 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
16, 16, ' 3/7/2014', ' 10/14/2019', 'Delivered', 9.99
);

/* INSERT QUERY NO: 17 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
17, 17, ' 8/20/2007', ' 3/23/2011', 'Pending', 17.99
);

/* INSERT QUERY NO: 18 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
18, 18, ' 4/9/2004', ' 12/12/2006', 'Delivered', 37.98
);

/* INSERT QUERY NO: 19 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
19, 19, ' 1/5/2013', ' 5/6/2018', 'Delivered', 75.96
);

/* INSERT QUERY NO: 20 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
20, 20, ' 4/11/2010', ' 9/12/2012', 'Delivered', 75.96
);

/* INSERT QUERY NO: 21 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
21, 21, ' 3/31/2001', ' 10/8/2003', 'Delivered', 1441.29
);

/* INSERT QUERY NO: 22 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
22, 22, ' 5/9/2014', ' 4/28/2019', 'Shipped', 38.97
);

/* INSERT QUERY NO: 23 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
23, 23, ' 3/30/2005', ' 10/1/2014', 'Pending', 19.96
);

/* INSERT QUERY NO: 24 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
24, 24, ' 6/25/2005', ' 9/29/2011', 'Shipped', 9.98
);

/* INSERT QUERY NO: 25 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
25, 25, ' 10/19/2003', ' 12/5/2016', 'Delivered', 24.95
);

/* INSERT QUERY NO: 26 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
26, 26, ' 2/14/2006', ' 11/29/2015', 'Shipped', 125.2
);

/* INSERT QUERY NO: 27 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
27, 27, ' 5/11/2003', ' 2/21/2008', 'Shipped', 31.78
);

/* INSERT QUERY NO: 28 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
28, 28, ' 2/1/2015', ' 1/25/2017', 'Delivered', 64.5
);

/* INSERT QUERY NO: 29 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
29, 29, ' 1/15/2015', ' 5/27/2020', 'Delivered', 7.99
);

/* INSERT QUERY NO: 30 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
30, 30, ' 9/22/2007', ' 5/18/2014', 'Delivered', 888.9
);

/* INSERT QUERY NO: 31 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
31, 31, ' 9/2/2002', ' 8/9/2005', 'Pending', 59.5
);

/* INSERT QUERY NO: 32 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
32, 32, ' 8/1/2001', ' 12/2/2006', 'Shipped', 89.25
);

/* INSERT QUERY NO: 33 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
33, 33, ' 11/10/2007', ' 7/27/2020', 'Delivered', 52.18
);

/* INSERT QUERY NO: 34 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
34, 34, ' 8/2/2014', ' 1/5/2015', 'Delivered', 34
);

/* INSERT QUERY NO: 35 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
35, 35, ' 1/1/2002', ' 7/16/2008', 'Delivered', 49.95
);

/* INSERT QUERY NO: 36 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
36, 36, ' 5/27/2012', ' 12/25/2015', 'Shipped', 114.95
);

/* INSERT QUERY NO: 37 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
37, 37, ' 9/21/2004', ' 12/3/2008', 'Shipped', 934.24
);

/* INSERT QUERY NO: 38 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
38, 38, ' 3/4/2006', ' 10/7/2009', 'Delivered', 77.5
);

/* INSERT QUERY NO: 39 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
39, 39, ' 8/20/2003', ' 1/22/2010', 'Shipped', 62
);

/* INSERT QUERY NO: 40 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
40, 40, ' 2/20/2002', ' 1/17/2014', 'Shipped', 104.97
);

/* INSERT QUERY NO: 41 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
41, 41, ' 7/7/2002', ' 5/12/2008', 'Delivered', 110.4
);

/* INSERT QUERY NO: 42 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
42, 42, ' 8/14/2010', ' 12/7/2014', 'Delivered', 4.88
);

/* INSERT QUERY NO: 43 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
43, 43, ' 12/3/2012', ' 11/19/2018', 'Pending', 186.9
);

/* INSERT QUERY NO: 44 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
44, 44, ' 9/17/2003', ' 12/30/2013', 'Shipped', 9
);

/* INSERT QUERY NO: 45 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
45, 45, ' 10/27/2009', ' 1/5/2015', 'Shipped', 31
);

/* INSERT QUERY NO: 46 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
46, 46, ' 5/8/2010', ' 3/29/2018', 'Shipped', 19.5
);

/* INSERT QUERY NO: 47 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
47, 47, ' 7/8/2011', ' 11/26/2017', 'Delivered', 15.5
);

/* INSERT QUERY NO: 48 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
48, 48, ' 4/30/2002', ' 3/22/2014', 'Delivered', 77.5
);

/* INSERT QUERY NO: 49 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
49, 49, ' 4/17/2003', ' 6/5/2017', 'Pending', 31
);

/* INSERT QUERY NO: 50 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
50, 50, ' 9/26/2001', ' 3/16/2016', 'Shipped', 30
);

/* INSERT QUERY NO: 51 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
51, 51, ' 5/13/2020', ' 5/16/2020', 'Shipped', 15.5
);

/* INSERT QUERY NO: 52 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
52, 52, ' 7/13/2004', ' 10/6/2017', 'Pending', 77.5
);

/* INSERT QUERY NO: 53 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
53, 53, ' 5/22/2008', ' 5/18/2014', 'Delivered', 31
);

/* INSERT QUERY NO: 54 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
54, 54, ' 3/5/2003', ' 11/22/2019', 'Shipped', 60
);

/* INSERT QUERY NO: 55 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
55, 55, ' 7/5/2004', ' 3/28/2016', 'Delivered', 27
);

/* INSERT QUERY NO: 56 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
56, 56, ' 8/17/2008', ' 6/20/2017', 'Delivered', 18
);

/* INSERT QUERY NO: 57 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
57, 57, ' 10/6/2008', ' 8/19/2013', 'Shipped', 51.96
);

/* INSERT QUERY NO: 58 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
58, 58, ' 10/27/2001', ' 7/28/2015', 'Shipped', 129.95
);

/* INSERT QUERY NO: 59 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
59, 59, ' 2/21/2005', ' 6/9/2006', 'Shipped', 9.99
);

/* INSERT QUERY NO: 60 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
60, 60, ' 10/15/2001', ' 8/27/2012', 'Delivered', 32
);

/* INSERT QUERY NO: 61 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
61, 61, ' 4/1/2005', ' 1/4/2011', 'Shipped', 15
);

/* INSERT QUERY NO: 62 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
62, 62, ' 8/16/2001', ' 2/17/2010', 'Shipped', 25.98
);

/* INSERT QUERY NO: 63 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
63, 63, ' 5/1/2013', ' 5/2/2020', 'Delivered', 759.8
);

/* INSERT QUERY NO: 64 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
64, 64, ' 5/15/2012', ' 5/27/2014', 'Delivered', 49.95
);

/* INSERT QUERY NO: 65 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
65, 65, ' 4/19/2009', ' 12/27/2014', 'Shipped', 547.96
);

/* INSERT QUERY NO: 66 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
66, 66, ' 9/1/2006', ' 11/5/2011', 'Delivered', 34.95
);

/* INSERT QUERY NO: 67 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
67, 67, ' 8/24/2001', ' 4/26/2015', 'Delivered', 488
);

/* INSERT QUERY NO: 68 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
68, 68, ' 6/22/2001', ' 10/2/2012', 'Delivered', 10.95
);

/* INSERT QUERY NO: 69 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
69, 69, ' 7/25/2012', ' 2/17/2016', 'Delivered', 54.95
);

/* INSERT QUERY NO: 70 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
70, 70, ' 10/11/2007', ' 10/19/2014', 'Shipped', 215.92
);

/* INSERT QUERY NO: 71 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
71, 71, ' 11/22/2004', ' 4/10/2009', 'Shipped', 74.97
);

/* INSERT QUERY NO: 72 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
72, 72, ' 9/23/2009', ' 1/6/2011', 'Shipped', 11.97
);

/* INSERT QUERY NO: 73 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
73, 73, ' 5/5/2005', ' 8/20/2008', 'Delivered', 53.97
);

/* INSERT QUERY NO: 74 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
74, 74, ' 11/5/2002', ' 5/19/2006', 'Delivered', 28.3
);

/* INSERT QUERY NO: 75 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
75, 75, ' 10/17/2014', ' 6/17/2018', 'Shipped', 21.98
);

/* INSERT QUERY NO: 76 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
76, 76, ' 12/26/2007', ' 1/10/2013', 'Delivered', 83.8
);

/* INSERT QUERY NO: 77 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
77, 77, ' 1/18/2009', ' 9/10/2018', 'Delivered', 3.99
);

/* INSERT QUERY NO: 78 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
78, 78, ' 8/31/2003', ' 12/13/2019', 'Delivered', 19.98
);

/* INSERT QUERY NO: 79 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
79, 79, ' 9/28/2008', ' 9/28/2013', 'Shipped', 65.9
);

/* INSERT QUERY NO: 80 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
80, 80, ' 7/13/2011', ' 11/1/2015', 'Shipped', 74.97
);

/* INSERT QUERY NO: 81 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
81, 81, ' 5/4/2001', ' 5/20/2018', 'Pending', 219.8
);

/* INSERT QUERY NO: 82 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
82, 82, ' 8/24/2012', ' 10/6/2016', 'Delivered', 45.98
);

/* INSERT QUERY NO: 83 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
83, 83, ' 6/12/2006', ' 3/24/2008', 'Delivered', 4310
);

/* INSERT QUERY NO: 84 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
84, 84, ' 12/31/2008', ' 11/17/2019', 'Pending', 41.95
);

/* INSERT QUERY NO: 85 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
85, 85, ' 6/29/2009', ' 5/17/2011', 'Shipped', 59.96
);

/* INSERT QUERY NO: 86 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
86, 86, ' 10/12/2006', ' 9/26/2015', 'Pending', 139.75
);

/* INSERT QUERY NO: 87 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
87, 87, ' 10/5/2010', ' 4/6/2011', 'Delivered', 63.98
);

/* INSERT QUERY NO: 88 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
88, 88, ' 10/15/2004', ' 5/29/2010', 'Pending', 85.9
);

/* INSERT QUERY NO: 89 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
89, 89, ' 3/23/2002', ' 9/28/2011', 'Delivered', 11.99
);

/* INSERT QUERY NO: 90 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
90, 90, ' 1/25/2005', ' 1/24/2020', 'Shipped', 159.8
);

/* INSERT QUERY NO: 91 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
91, 91, ' 11/7/2016', ' 9/8/2019', 'Delivered', 17136.9
);

/* INSERT QUERY NO: 92 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
92, 92, ' 6/21/2005', ' 7/3/2019', 'Delivered', 47.96
);

/* INSERT QUERY NO: 93 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
93, 93, ' 3/20/2011', ' 1/6/2019', 'Pending', 31.47
);

/* INSERT QUERY NO: 94 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
94, 94, ' 4/12/2009', ' 1/4/2012', 'Shipped', 6
);

/* INSERT QUERY NO: 95 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
95, 95, ' 9/10/2005', ' 12/10/2013', 'Pending', 174.95
);

/* INSERT QUERY NO: 96 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
96, 96, ' 12/15/2002', ' 10/7/2008', 'Shipped', 68.97
);

/* INSERT QUERY NO: 97 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
97, 97, ' 3/16/2005', ' 7/28/2014', 'Shipped', 1.99
);

/* INSERT QUERY NO: 98 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
98, 98, ' 12/30/2003', ' 8/18/2018', 'Delivered', 56
);

/* INSERT QUERY NO: 99 */
INSERT INTO "order"(order_id, buyer_id, order_date, shipped_date, status, total)
VALUES
(
99, 99, ' 8/13/2003', ' 12/6/2004', 'Pending', 111.96
);