﻿/* INSERT QUERY NO: 1 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
1, 191, ' 5/28/2007', 'Amazing! I would recommend this seller to anyone. Five stars!', 5
);

/* INSERT QUERY NO: 2 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
2, 128, ' 11/11/2007', 'This seller had amazing customer service.', 4.8
);

/* INSERT QUERY NO: 3 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
3, 86, ' 11/17/2019', 'This seller was great and helped me with my order!', 4.5
);

/* INSERT QUERY NO: 4 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
4, 48, ' 2/17/2008', 'They were difficult to contact.', 3.5
);

/* INSERT QUERY NO: 5 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
5, 185, ' 4/8/2007', 'Amazing! I would recommend this seller to anyone. Five stars!', 5
);

/* INSERT QUERY NO: 6 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
6, 59, ' 1/1/2013', 'They were difficult to contact.', 3.5
);

/* INSERT QUERY NO: 7 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
7, 115, ' 8/9/2019', 'This seller had amazing customer service.', 4.8
);

/* INSERT QUERY NO: 8 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
8, 195, ' 3/14/2013', 'Amazing! I would recommend this seller to anyone. Five stars!', 5
);

/* INSERT QUERY NO: 9 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
9, 6, ' 11/16/2015', 'They were difficult to contact.', 3.5
);

/* INSERT QUERY NO: 10 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
10, 156, ' 4/20/2011', 'Amazing! I would recommend this seller to anyone. Five stars!', 5
);

/* INSERT QUERY NO: 11 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
11, 132, ' 7/8/2019', 'This seller never delivered my items!', 2
);

/* INSERT QUERY NO: 12 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
12, 150, ' 1/29/2007', 'Amazing! I would recommend this seller to anyone. Five stars!', 5
);

/* INSERT QUERY NO: 13 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
13, 171, ' 7/15/2017', 'Amazing! I would recommend this seller to anyone. Five stars!', 5
);

/* INSERT QUERY NO: 14 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
14, 145, ' 4/23/2012', 'This seller never delivered my items!', 2
);

/* INSERT QUERY NO: 15 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
15, 144, ' 12/26/2011', 'This seller never delivered my items!', 2
);

/* INSERT QUERY NO: 16 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
16, 90, ' 10/14/2019', 'This seller had amazing customer service.', 4.8
);

/* INSERT QUERY NO: 17 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
17, 60, ' 3/23/2011', 'This seller was great and helped me with my order!', 4.5
);

/* INSERT QUERY NO: 18 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
18, 138, ' 12/12/2006', 'This seller never delivered my items!', 2
);

/* INSERT QUERY NO: 19 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
19, 152, ' 5/6/2018', 'Amazing! I would recommend this seller to anyone. Five stars!', 5
);

/* INSERT QUERY NO: 20 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
20, 87, ' 9/12/2012', 'This seller was great and helped me with my order!', 4.5
);

/* INSERT QUERY NO: 21 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
21, 68, ' 10/8/2003', 'This seller was great and helped me with my order!', 4.5
);

/* INSERT QUERY NO: 22 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
22, 190, ' 4/28/2019', 'Amazing! I would recommend this seller to anyone. Five stars!', 5
);

/* INSERT QUERY NO: 23 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
23, 81, ' 10/1/2014', 'This seller was great and helped me with my order!', 4.5
);

/* INSERT QUERY NO: 24 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
24, 69, ' 9/29/2011', 'This seller was great and helped me with my order!', 4.5
);

/* INSERT QUERY NO: 25 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
25, 77, ' 12/5/2016', 'This seller was great and helped me with my order!', 4.5
);

/* INSERT QUERY NO: 26 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
26, 140, ' 11/29/2015', 'This seller never delivered my items!', 2
);

/* INSERT QUERY NO: 27 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
27, 160, ' 2/21/2008', 'Amazing! I would recommend this seller to anyone. Five stars!', 5
);

/* INSERT QUERY NO: 28 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
28, 165, ' 1/25/2017', 'Amazing! I would recommend this seller to anyone. Five stars!', 5
);

/* INSERT QUERY NO: 29 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
29, 67, ' 5/27/2020', 'This seller was great and helped me with my order!', 4.5
);

/* INSERT QUERY NO: 30 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
30, 37, ' 5/18/2014', 'They were difficult to contact.', 3.5
);

/* INSERT QUERY NO: 31 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
31, 25, ' 8/9/2005', 'They were difficult to contact.', 3.5
);

/* INSERT QUERY NO: 32 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
32, 67, ' 12/2/2006', 'This seller was great and helped me with my order!', 4.5
);

/* INSERT QUERY NO: 33 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
33, 113, ' 7/27/2020', 'This seller had amazing customer service.', 4.8
);

/* INSERT QUERY NO: 34 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
34, 165, ' 1/5/2015', 'Amazing! I would recommend this seller to anyone. Five stars!', 5
);

/* INSERT QUERY NO: 35 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
35, 69, ' 7/16/2008', 'This seller was great and helped me with my order!', 4.5
);

/* INSERT QUERY NO: 36 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
36, 32, ' 12/25/2015', 'They were difficult to contact.', 3.5
);

/* INSERT QUERY NO: 37 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
37, 149, ' 12/3/2008', 'This seller never delivered my items!', 2
);

/* INSERT QUERY NO: 38 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
38, 114, ' 10/7/2009', 'This seller had amazing customer service.', 4.8
);

/* INSERT QUERY NO: 39 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
39, 88, ' 1/22/2010', 'This seller was great and helped me with my order!', 4.5
);

/* INSERT QUERY NO: 40 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
40, 200, ' 1/17/2014', 'Amazing! I would recommend this seller to anyone. Five stars!', 5
);

/* INSERT QUERY NO: 41 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
41, 36, ' 5/12/2008', 'They were difficult to contact.', 3.5
);

/* INSERT QUERY NO: 42 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
42, 98, ' 12/7/2014', 'This seller had amazing customer service.', 4.8
);

/* INSERT QUERY NO: 43 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
43, 193, ' 11/19/2018', 'Amazing! I would recommend this seller to anyone. Five stars!', 5
);

/* INSERT QUERY NO: 44 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
44, 128, ' 12/30/2013', 'This seller had amazing customer service.', 4.8
);

/* INSERT QUERY NO: 45 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
45, 57, ' 1/5/2015', 'They were difficult to contact.', 3.5
);

/* INSERT QUERY NO: 46 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
46, 56, ' 3/29/2018', 'They were difficult to contact.', 3.5
);

/* INSERT QUERY NO: 47 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
47, 22, ' 11/26/2017', 'They were difficult to contact.', 3.5
);

/* INSERT QUERY NO: 48 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
48, 55, ' 3/22/2014', 'They were difficult to contact.', 3.5
);

/* INSERT QUERY NO: 49 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
49, 174, ' 6/5/2017', 'Amazing! I would recommend this seller to anyone. Five stars!', 5
);

/* INSERT QUERY NO: 50 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
50, 178, ' 3/16/2016', 'Amazing! I would recommend this seller to anyone. Five stars!', 5
);

/* INSERT QUERY NO: 51 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
51, 94, ' 5/16/2020', 'This seller had amazing customer service.', 4.8
);

/* INSERT QUERY NO: 52 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
52, 22, ' 3/23/2011', 'They were difficult to contact.', 3.5
);

/* INSERT QUERY NO: 53 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
53, 24, ' 5/18/2014', 'They were difficult to contact.', 3.5
);

/* INSERT QUERY NO: 54 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
54, 111, ' 11/22/2019', 'This seller had amazing customer service.', 4.8
);

/* INSERT QUERY NO: 55 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
55, 134, ' 3/28/2016', 'This seller never delivered my items!', 2
);

/* INSERT QUERY NO: 56 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
56, 146, ' 6/20/2017', 'This seller never delivered my items!', 2
);

/* INSERT QUERY NO: 57 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
57, 88, ' 5/18/2014', 'This seller was great and helped me with my order!', 4.5
);

/* INSERT QUERY NO: 58 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
58, 195, ' 7/28/2015', 'Amazing! I would recommend this seller to anyone. Five stars!', 5
);

/* INSERT QUERY NO: 59 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
59, 110, ' 6/9/2006', 'This seller had amazing customer service.', 4.8
);

/* INSERT QUERY NO: 60 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
60, 68, ' 8/27/2012', 'This seller was great and helped me with my order!', 4.5
);

/* INSERT QUERY NO: 61 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
61, 8, ' 1/4/2011', 'They were difficult to contact.', 3.5
);

/* INSERT QUERY NO: 62 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
62, 126, ' 2/17/2010', 'This seller had amazing customer service.', 4.8
);

/* INSERT QUERY NO: 63 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
63, 199, ' 5/2/2020', 'Amazing! I would recommend this seller to anyone. Five stars!', 5
);

/* INSERT QUERY NO: 64 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
64, 116, ' 5/27/2014', 'This seller had amazing customer service.', 4.8
);

/* INSERT QUERY NO: 65 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
65, 46, ' 12/27/2014', 'They were difficult to contact.', 3.5
);

/* INSERT QUERY NO: 66 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
66, 10, ' 11/5/2011', 'They were difficult to contact.', 3.5
);

/* INSERT QUERY NO: 67 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
67, 143, ' 4/26/2015', 'This seller never delivered my items!', 2
);

/* INSERT QUERY NO: 68 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
68, 111, ' 10/2/2012', 'This seller had amazing customer service.', 4.8
);

/* INSERT QUERY NO: 69 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
69, 150, ' 2/17/2016', 'Amazing! I would recommend this seller to anyone. Five stars!', 5
);

/* INSERT QUERY NO: 70 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
70, 92, ' 10/19/2014', 'This seller had amazing customer service.', 4.8
);

/* INSERT QUERY NO: 71 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
71, 126, ' 4/10/2009', 'This seller had amazing customer service.', 4.8
);

/* INSERT QUERY NO: 72 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
72, 66, ' 1/6/2011', 'This seller was great and helped me with my order!', 4.5
);

/* INSERT QUERY NO: 73 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
73, 165, ' 8/20/2008', 'Amazing! I would recommend this seller to anyone. Five stars!', 5
);

/* INSERT QUERY NO: 74 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
74, 175, ' 5/19/2006', 'Amazing! I would recommend this seller to anyone. Five stars!', 5
);

/* INSERT QUERY NO: 75 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
75, 42, ' 6/17/2018', 'They were difficult to contact.', 3.5
);

/* INSERT QUERY NO: 76 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
76, 98, ' 1/10/2013', 'This seller had amazing customer service.', 4.8
);

/* INSERT QUERY NO: 77 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
77, 140, ' 9/10/2018', 'This seller never delivered my items!', 2
);

/* INSERT QUERY NO: 78 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
78, 194, ' 12/13/2019', 'Amazing! I would recommend this seller to anyone. Five stars!', 5
);

/* INSERT QUERY NO: 79 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
79, 55, ' 9/28/2013', 'They were difficult to contact.', 3.5
);

/* INSERT QUERY NO: 80 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
80, 68, ' 11/1/2015', 'This seller was great and helped me with my order!', 4.5
);

/* INSERT QUERY NO: 81 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
81, 161, ' 5/20/2018', 'Amazing! I would recommend this seller to anyone. Five stars!', 5
);

/* INSERT QUERY NO: 82 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
82, 143, ' 10/6/2016', 'This seller never delivered my items!', 2
);

/* INSERT QUERY NO: 83 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
83, 191, ' 3/24/2008', 'Amazing! I would recommend this seller to anyone. Five stars!', 5
);

/* INSERT QUERY NO: 84 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
84, 192, ' 11/17/2019', 'Amazing! I would recommend this seller to anyone. Five stars!', 5
);

/* INSERT QUERY NO: 85 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
85, 94, ' 12/30/2013', 'This seller had amazing customer service.', 4.8
);

/* INSERT QUERY NO: 86 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
86, 164, ' 9/26/2015', 'Amazing! I would recommend this seller to anyone. Five stars!', 5
);

/* INSERT QUERY NO: 87 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
87, 162, ' 4/6/2011', 'Amazing! I would recommend this seller to anyone. Five stars!', 5
);

/* INSERT QUERY NO: 88 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
88, 138, ' 5/29/2010', 'This seller never delivered my items!', 2
);

/* INSERT QUERY NO: 89 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
89, 157, ' 9/28/2011', 'Amazing! I would recommend this seller to anyone. Five stars!', 5
);

/* INSERT QUERY NO: 90 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
90, 107, ' 1/24/2020', 'This seller had amazing customer service.', 4.8
);

/* INSERT QUERY NO: 91 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
91, 96, ' 9/8/2019', 'This seller had amazing customer service.', 4.8
);

/* INSERT QUERY NO: 92 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
92, 132, ' 7/3/2019', 'This seller never delivered my items!', 2
);

/* INSERT QUERY NO: 93 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
93, 130, ' 1/6/2019', 'This seller never delivered my items!', 2
);

/* INSERT QUERY NO: 94 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
94, 10, ' 1/4/2012', 'They were difficult to contact.', 3.5
);

/* INSERT QUERY NO: 95 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
95, 122, ' 12/10/2013', 'This seller had amazing customer service.', 4.8
);

/* INSERT QUERY NO: 96 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
96, 30, ' 10/7/2008', 'They were difficult to contact.', 3.5
);

/* INSERT QUERY NO: 97 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
97, 114, ' 7/28/2014', 'This seller had amazing customer service.', 4.8
);

/* INSERT QUERY NO: 98 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
98, 18, ' 8/18/2018', 'They were difficult to contact.', 3.5
);

/* INSERT QUERY NO: 99 */
INSERT INTO Review_Seller(buyer_id, seller_id, date, review_text, rating)
VALUES
(
99, 14, ' 12/6/2004', 'They were difficult to contact.', 3.5
);