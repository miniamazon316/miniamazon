﻿/* INSERT QUERY NO: 1 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
1, 984, 191, 3, 9.99
);

/* INSERT QUERY NO: 2 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
2, 985, 128, 5, 14.99
);

/* INSERT QUERY NO: 3 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
3, 986, 86, 1, 9.99
);

/* INSERT QUERY NO: 4 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
4, 987, 48, 1, 11.99
);

/* INSERT QUERY NO: 5 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
5, 988, 185, 2, 5.99
);

/* INSERT QUERY NO: 6 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
6, 989, 59, 4, 14.99
);

/* INSERT QUERY NO: 7 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
7, 990, 115, 4, 12.6
);

/* INSERT QUERY NO: 8 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
8, 991, 195, 3, 4.99
);

/* INSERT QUERY NO: 9 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
9, 992, 6, 4, 6.99
);

/* INSERT QUERY NO: 10 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
10, 993, 156, 5, 34.99
);

/* INSERT QUERY NO: 11 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
11, 994, 132, 4, 14.99
);

/* INSERT QUERY NO: 12 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
12, 995, 150, 2, 35
);

/* INSERT QUERY NO: 13 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
13, 996, 171, 4, 10.99
);

/* INSERT QUERY NO: 14 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
14, 997, 145, 2, 8.99
);

/* INSERT QUERY NO: 15 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
15, 998, 144, 2, 24
);

/* INSERT QUERY NO: 16 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
16, 999, 90, 1, 9.99
);

/* INSERT QUERY NO: 17 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
17, 660, 60, 1, 17.99
);

/* INSERT QUERY NO: 18 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
18, 661, 138, 2, 18.99
);

/* INSERT QUERY NO: 19 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
19, 662, 152, 4, 18.99
);

/* INSERT QUERY NO: 20 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
20, 663, 87, 4, 18.99
);

/* INSERT QUERY NO: 21 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
21, 664, 68, 3, 480.43
);

/* INSERT QUERY NO: 22 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
22, 665, 190, 3, 12.99
);

/* INSERT QUERY NO: 23 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
23, 666, 81, 4, 4.99
);

/* INSERT QUERY NO: 24 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
24, 667, 69, 2, 4.99
);

/* INSERT QUERY NO: 25 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
25, 668, 77, 5, 4.99
);

/* INSERT QUERY NO: 26 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
26, 669, 140, 4, 31.3
);

/* INSERT QUERY NO: 27 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
27, 670, 160, 1, 31.78
);

/* INSERT QUERY NO: 28 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
28, 671, 165, 2, 32.25
);

/* INSERT QUERY NO: 29 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
29, 672, 67, 1, 7.99
);

/* INSERT QUERY NO: 30 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
30, 673, 37, 2, 444.45
);

/* INSERT QUERY NO: 31 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
31, 674, 25, 2, 29.75
);

/* INSERT QUERY NO: 32 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
32, 675, 67, 3, 29.75
);

/* INSERT QUERY NO: 33 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
33, 676, 113, 2, 26.09
);

/* INSERT QUERY NO: 34 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
34, 677, 165, 4, 8.5
);

/* INSERT QUERY NO: 35 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
35, 678, 69, 5, 9.99
);

/* INSERT QUERY NO: 36 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
36, 679, 32, 5, 22.99
);

/* INSERT QUERY NO: 37 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
37, 680, 149, 4, 233.56
);

/* INSERT QUERY NO: 38 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
38, 681, 114, 5, 15.5
);

/* INSERT QUERY NO: 39 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
39, 682, 88, 4, 15.5
);

/* INSERT QUERY NO: 40 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
40, 683, 200, 3, 34.99
);

/* INSERT QUERY NO: 41 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
41, 684, 36, 3, 36.8
);

/* INSERT QUERY NO: 42 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
42, 685, 98, 1, 4.88
);

/* INSERT QUERY NO: 43 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
43, 686, 193, 5, 37.38
);

/* INSERT QUERY NO: 44 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
44, 687, 128, 1, 9
);

/* INSERT QUERY NO: 45 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
45, 688, 57, 2, 15.5
);

/* INSERT QUERY NO: 46 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
46, 689, 56, 3, 6.5
);

/* INSERT QUERY NO: 47 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
47, 690, 22, 1, 15.5
);

/* INSERT QUERY NO: 48 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
48, 691, 55, 5, 15.5
);

/* INSERT QUERY NO: 49 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
49, 692, 174, 2, 15.5
);

/* INSERT QUERY NO: 50 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
50, 693, 178, 4, 7.5
);

/* INSERT QUERY NO: 51 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
51, 694, 94, 1, 15.5
);

/* INSERT QUERY NO: 52 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
52, 695, 22, 5, 15.5
);

/* INSERT QUERY NO: 53 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
53, 696, 24, 2, 15.5
);

/* INSERT QUERY NO: 54 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
54, 697, 111, 5, 12
);

/* INSERT QUERY NO: 55 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
55, 698, 134, 3, 9
);

/* INSERT QUERY NO: 56 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
56, 699, 146, 2, 9
);

/* INSERT QUERY NO: 57 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
57, 700, 88, 4, 12.99
);

/* INSERT QUERY NO: 58 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
58, 57, 195, 5, 25.99
);

/* INSERT QUERY NO: 59 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
59, 58, 110, 1, 9.99
);

/* INSERT QUERY NO: 60 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
60, 59, 68, 2, 16
);

/* INSERT QUERY NO: 61 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
61, 60, 8, 2, 7.5
);

/* INSERT QUERY NO: 62 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
62, 61, 126, 2, 12.99
);

/* INSERT QUERY NO: 63 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
63, 62, 199, 4, 189.95
);

/* INSERT QUERY NO: 64 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
64, 63, 116, 1, 49.95
);

/* INSERT QUERY NO: 65 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
65, 64, 46, 4, 136.99
);

/* INSERT QUERY NO: 66 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
66, 65, 10, 1, 34.95
);

/* INSERT QUERY NO: 67 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
67, 66, 143, 2, 244
);

/* INSERT QUERY NO: 68 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
68, 67, 111, 1, 10.95
);

/* INSERT QUERY NO: 69 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
69, 68, 150, 5, 10.99
);

/* INSERT QUERY NO: 70 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
70, 69, 92, 4, 53.98
);

/* INSERT QUERY NO: 71 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
71, 70, 126, 3, 24.99
);

/* INSERT QUERY NO: 72 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
72, 71, 66, 3, 3.99
);

/* INSERT QUERY NO: 73 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
73, 72, 165, 3, 17.99
);

/* INSERT QUERY NO: 74 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
74, 73, 175, 2, 14.15
);

/* INSERT QUERY NO: 75 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
75, 74, 42, 2, 10.99
);

/* INSERT QUERY NO: 76 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
76, 75, 98, 4, 20.95
);

/* INSERT QUERY NO: 77 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
77, 76, 140, 1, 3.99
);

/* INSERT QUERY NO: 78 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
78, 77, 194, 2, 9.99
);

/* INSERT QUERY NO: 79 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
79, 78, 55, 2, 32.95
);

/* INSERT QUERY NO: 80 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
80, 79, 68, 3, 24.99
);

/* INSERT QUERY NO: 81 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
81, 80, 161, 4, 54.95
);

/* INSERT QUERY NO: 82 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
82, 81, 143, 2, 22.99
);

/* INSERT QUERY NO: 83 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
83, 82, 191, 4, 1077.5
);

/* INSERT QUERY NO: 84 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
84, 83, 192, 1, 41.95
);

/* INSERT QUERY NO: 85 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
85, 84, 94, 4, 14.99
);

/* INSERT QUERY NO: 86 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
86, 85, 164, 5, 27.95
);

/* INSERT QUERY NO: 87 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
87, 86, 162, 2, 31.99
);

/* INSERT QUERY NO: 88 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
88, 87, 138, 2, 42.95
);

/* INSERT QUERY NO: 89 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
89, 88, 157, 1, 11.99
);

/* INSERT QUERY NO: 90 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
90, 89, 107, 4, 39.95
);

/* INSERT QUERY NO: 91 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
91, 90, 96, 3, 5712.3
);

/* INSERT QUERY NO: 92 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
92, 91, 132, 4, 11.99
);

/* INSERT QUERY NO: 93 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
93, 92, 130, 3, 10.49
);

/* INSERT QUERY NO: 94 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
94, 93, 10, 3, 2
);

/* INSERT QUERY NO: 95 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
95, 94, 122, 5, 34.99
);

/* INSERT QUERY NO: 96 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
96, 95, 30, 3, 22.99
);

/* INSERT QUERY NO: 97 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
97, 96, 114, 1, 1.99
);

/* INSERT QUERY NO: 98 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
98, 97, 18, 5, 11.2
);

/* INSERT QUERY NO: 99 */
INSERT INTO Order_Item(order_id, item_id, seller_id, quantity, price_each)
VALUES
(
99, 98, 14, 4, 27.99
);