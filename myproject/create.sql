Create Table Profile(
    user_id serial primary key,
    email varchar(200),
    phone varchar(20),
    name varchar(200),
    balance numeric default 0.0,
    address varchar(500)
);

create Table Person(
    user_id integer primary key,
    username varchar(200),
    password varchar(200),
    Foreign Key (user_id) references Profile(user_id)
);

Create Table Buyer(
    buyer_id serial primary key,
    user_id integer references Profile(user_id)
);

Create Table Seller(
    seller_id serial primary key,
    user_id integer references Profile(user_id)
);

create Table Category (
    category_name varchar(200) primary key not null
);

Create Table Cart(
    cart_id serial primary key,
    buyer_id integer references Buyer(buyer_id)
);

create table Warehouse(
    warehouse_id serial primary key not null,
    address varchar(500),
    name varchar(200)
);

Create Table Item(
    item_id serial primary key not null,
    item_name varchar not null,
    category_name varchar(200) references Category(category_name),
    description varchar,
    avg_rating numeric,
    item_link varchar
);

create Table Item_For_Sale(
    item_id integer not null
        references Item(item_id),
    seller_id integer not null
        references Seller(seller_id),
    price numeric not null check (price > 0),
    primary key(item_id,seller_id),
    quantity integer,
    warehouse integer not null
    -- quantity must be <= to warehouse quantity for item
);

create Table Cart_Item(
    cart_id integer not null references Cart(cart_id),
    item_id integer not null
        references Item(item_id),
    seller_id integer not null
        references Seller(seller_id),
    price numeric,
    quantity integer,
    primary key(cart_id,item_id,seller_id)
);

Create Table "order" (
    order_id serial primary key,
    buyer_id integer not null
        references Buyer(buyer_id),
    order_date date,
    shipped_date date,
    status varchar(20) check (status in ('Shipped','Pending','Delivered')),
    total numeric
);


create table Order_Item(
    order_id integer not null
        references "order"(order_id),
    item_id integer not null
        references Item(item_id),
    seller_id integer not null
        references Seller(seller_id),
    quantity integer,
    price_each numeric not null check (price_each > 0),
    primary key(order_id,item_id,seller_id)
);

Create Table Seller_history(
    item_id integer not null
        references Item(item_id),
    seller_id integer not null
        references Seller(seller_id),
    order_id integer not null
        references "order"(order_id),
    quantity integer,
    date_purchased date,
    price numeric not null check (price > 0),
    primary key(seller_id,item_id,order_id)
);

create table Review_Seller(
    buyer_id integer not null
        references Buyer(buyer_id),
    seller_id integer not null
        references Seller(seller_id),
    date date not null,
    review_text varchar,
    rating numeric,
    primary key(buyer_id,seller_id,date)
);

create table Review_Item(
    buyer_id integer not null
        references Buyer(buyer_id),
    item_id integer not null
        references Item(item_id),
    date date not null,
    review_text varchar,
    rating numeric,
    primary key(buyer_id,item_id,date)
);

create table Warehouse_Inventory(
    warehouse_id integer not null
        references Warehouse(warehouse_id),
    item_id integer not null
        references Item(item_id),
    primary key(warehouse_id,item_id),
    quantity integer,
    price numeric
);

create table Shipments(
    warehouse_id integer primary key not null
        references Warehouse(warehouse_id),
    order_id integer references "order"(order_id),
    item_id integer references Item(item_id)
);

--this trigger checks that a seller cannot sell a larger quantity of an
--item than the warehouse has
-- this trigger has an issue / isnt fully working
-- Create function SellerQuantity_WarehouseQuantity() returns trigger as $$
-- BEGIN
-- if exists (select * from Warehouse_Inventory
-- where Warehouse_Inventory.item_id = NEW.item_id
-- and Warehouse_Inventory.quantity < NEW.quantity)
-- then raise exception 'Warehouse % does not have more than % of item %.',
-- Warehouse_Inventory.warehouse_id, Warehouse_Inventory.quantity,
-- Warehouse_Inventory.item_id;
-- end if;
--     Return NEW;
-- End;
-- $$ LANGUAGE plpgsql;

-- Create Trigger SellerQuantity_WarehouseQuantity
--     Before insert or update on ItemForSale
--     for each row
--     execute procedure SellerQuantity_WarehouseQuantity();

-- -- TODO:

--1.
-- create trigger to update item quantity in warehouse
--  if duplicate item is added to Item()

--2.
-- create trigger to remove item / update quantity of item from
-- warehouse if item is added to ItemForSale() (this means a seller is taking the item)
--also include constraint to check that quantity of item being
--added to ItemForSale() is not larger than quantity of item
-- in Warehouse_Inventory

--3.
--create trigger to remove item/update quantity
-- from ItemForSale() and SellingList() if it
--is added to Order_item() (this means the item was purchased)
--also add constraint that quantity of item being added
--to Order_item() is not greater than quantity of item
-- on SellingList()

--4.
--create constraint that shipped_date > order_date in Orders()

--5.
-- create trigger to update the average rating of items
--each time an new item is review
-- sample code below
-- Create Table Average_Ratings
--     as select item_id, avg(rating) average_rating
--     from Review_Item
--     group by item_id;
