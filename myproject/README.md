# MiniAmazon

## Important installation info:

When first in vcm@vcm-17302.vm.duke.edu:

sudo -i -u development

## Commands to run:

python3 -m venv env

source env/bin/activate

pip install -r requirements.txt

pip install psycopg2-binary

## Run project once in myproject directory:

python app.py


## Build database from create.sql and load.sql:
dropdb development

createdb development

psql development -f create.sql

psql development -f load.sql

