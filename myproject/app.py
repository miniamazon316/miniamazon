#!/usr/bin/python
# -*- coding: utf-8 -*-
from flask import Flask, render_template, redirect, url_for, request, \
    session, flash
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
import models
import forms
from flask_bootstrap import Bootstrap
from db import app, db
from sqlalchemy import desc, nullslast

# app = Flask(__name__)

Bootstrap(app)
app.secret_key = 's3cr3t'
app.config.from_object('config')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False


# db = SQLAlchemy(app, session_options={'autocommit': False})

@app.route('/allusers')
def all_users():
    people = db.session.query(models.Person).all()
    return render_template('all_users.html', peeps=people)


@app.route('/landing', methods=['GET', 'POST'])
def landing_page():
    if session is None:
        return redirect(url_for('signin'))
    form = forms.Search.form()
    cart_id = session.get('cart_id')
    results = \
        db.session.query(models.ItemForSale).filter(models.ItemForSale.item_id
            == models.Item.item_id, models.Item.item_name
            == form.search.data).all()
    user = \
        db.session.query(models.Profile).filter(models.Profile.user_id
            == session['user_id']).first()
    esults = \
        db.session.query(models.ItemForSale).filter(models.ItemForSale.item_id
            == models.Item.item_id,
            models.Item.item_name.ilike(f'%{form.search.data}%')).all()
    items = db.session.query(models.Item)
    categories = db.session.query(models.Category)
    if 'seller_id' in session.keys():
        seller_id = session['seller_id']
    else:
        seller_id = None
    select = request.form.get('category')
    categoryItems = \
        db.session.query(models.Item).filter(models.Item.category_name
            == select).order_by(nullslast(desc(models.Item.avg_rating))).limit(3)
    if form.validate_on_submit():
        return render_template('search_results.html', results=esults,
                               profile=user)
    return render_template(
        'index.html',
        seller_id=seller_id,
        items=categoryItems,
        category=select,
        cat=categories,
        form=form,
        data=results,
        cart=cart_id,
        session=session,
        profile=user,
        )


@app.route('/', methods=['GET', 'POST'])
def signin():
    form = forms.SignIn.form()
    if form.validate_on_submit():
        user = \
            db.session.query(models.Person).filter(models.Person.username
                == form.username.data).first()
        if not user:
            flash('user does not exist')
            return redirect(url_for('signin'))
        if not user.password == form.password.data:
            flash('wrong password, please try again')
            return redirect(url_for('signin'))

        # users = db.session.query(models.Person).filter(models.Person.username == form.username.data).all()
        # sub = None
        # for each in users:
        #    if each.username == form.username.data and each.password == form.password.data:
        #        sub = each
        #        seller = db.session.query(models.Seller).filter(models.Seller.seller_id == sub.user_id).all()
        # if sub:

        session.clear()
        session['user_id'] = user.user_id
        session['buyer_id'] = user.profile.buyer.buyer_id
        session['cart_id'] = user.profile.buyer.cart.cart_id
        if user.profile.seller is not None:
            session['seller_id'] = user.profile.seller.seller_id

            # session['profile'] = sub.profile.__dict__
            # session['buyer'] = session['profile'].get('buyer')
        # return render_template('profile.html', profile=user.profile)

            return redirect(url_for('landing_page'))

 # return render_template('login.html', form=form)

    return render_template('login.html', form=form)


@app.route('/signup', methods=['GET', 'POST'])
def signup():
    form = forms.Register.form()
    if form.validate_on_submit():
        if db.session.query(models.Person).filter(models.Person.username
                == form.username.data).first():
            flash('username already exists')
            return redirect(url_for('signup'))
        if db.session.query(models.Profile).filter(models.Profile.email
                == form.email.data).first():
            flash('email is already being used')
            return redirect(url_for('signup'))
        profile_max = \
            db.session.query(db.func.max(models.Profile.user_id)).scalar()
        new_profile = models.Profile(
            user_id=profile_max + 1,
            email=form.email.data,
            phone=form.phone.data,
            name=form.name.data,
            balance=0,
            address=form.address.data,
            )
        new_person = models.Person(user_id=new_profile.user_id,
                                   username=form.username.data,
                                   password=form.password.data)
        new_profile.person = new_person
        buyer_max = \
            db.session.query(db.func.max(models.Buyer.buyer_id)).scalar()
        new_buyer = models.Buyer(buyer_id=buyer_max + 1,
                                 user_id=new_profile.user_id)
        new_profile.buyer = new_buyer
        cart_max = \
            db.session.query(db.func.max(models.Cart.cart_id)).scalar()
        new_cart = models.Cart(cart_id=cart_max + 1,
                               buyer_id=new_buyer.buyer_id)
        new_buyer.cart = new_cart
        if form.seller.data:
            new_seller = models.Seller(seller_id=profile_max + 1,
                    user_id=profile_max + 1)
            db.session.add(new_seller)
        db.session.add(new_profile)
        db.session.add(new_person)
        db.session.add(new_buyer)
        db.session.add(new_cart)

        db.session.commit()

        return redirect(url_for('signin'))
    return render_template('signup.html', form=form)


@app.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('signin'))


@app.route('/reset_password', methods=['GET', 'POST'])
def resetPassword():
    form = forms.NewPassword.form()
    if form.validate_on_submit():
        users = \
            db.session.query(models.Person).filter(models.Person.username
                == form.username.data).all()
        sub = None
        for each in users:
            if each.username == form.username.data:
                sub = each
        if sub:
            sub.password = form.confirm_new_password.data
            db.session.commit()
            return redirect(url_for('signin'))
    return render_template('forgot_password.html', form=form)


@app.route('/addToCart')
def addToCart():
    item_id = int(request.args.get('item_id'))
    seller_id = int(request.args.get('seller_id'))
    cart_item = \
        db.session.query(models.Cart_Item).filter(models.Cart_Item.cart_id
            == session.get('cart_id'), models.Cart_Item.seller_id
            == seller_id, models.Cart_Item.item_id == item_id).first()
    item_for_sale = \
        db.session.query(models.ItemForSale).filter(models.ItemForSale.item_id
            == item_id, models.ItemForSale.seller_id
            == seller_id).first()
    if cart_item:
        if cart_item.quantity + 1 <= item_for_sale.quantity:
            cart_item.quantity = cart_item.quantity + 1
            db.session.commit()
        else:
            flash('Item quantity not updated: Not enough inventory',
                  'error')
    else:

    # product = Product.query.filter_by(id = productid).first()
    # cart = Cart.query.filter_by(product_id = productid).first()

        if 1 <= item_for_sale.quantity:
            new_cart_item = \
                models.Cart_Item(cart_id=session.get('cart_id'),
                                 item_id=item_id, seller_id=seller_id,
                                 price=item_for_sale.price, quantity=1)
            db.session.add(new_cart_item)
            db.session.commit()
        else:
            flash('Item not added to cart: Not enough inventory',
                  'error')

    return redirect(url_for('cart'))


@app.route('/cart', methods=['GET', 'POST'])
def cart():
    my_cart = db.session.query(models.Cart).filter(models.Cart.cart_id
            == session.get('cart_id')).first()
    cart_items = my_cart.items
    total = 0
    for cart_item in cart_items:
        total += cart_item.price * cart_item.quantity
    user = \
        db.session.query(models.Profile).filter(models.Profile.user_id
            == session['user_id']).first()
    if 'seller_id' in session.keys():
        seller_id = session['seller_id']
    else:
        seller_id = None

    return render_template(
        'cart.html',
        profile=user,
        cart_items=cart_items,
        cart=my_cart,
        total=total,
        seller_id=seller_id,
        )


@app.route('/removeSellerItem')
def removeSellerItem():
    item_id = int(request.args.get('item_id'))
    itemforsale = \
        db.session.query(models.ItemForSale).filter(models.ItemForSale.seller_id
            == session.get('seller_id'), models.ItemForSale.item_id
            == item_id).first()
    warehouse = itemforsale.warehouse
    if itemforsale.quantity > 1:
        quantity = itemforsale.quantity - 1
        itemforsale.quantity = quantity
    else:
        db.session.delete(itemforsale)
    warehouseitem = \
        db.session.query(models.Warehouse_Inventory).filter(models.Warehouse_Inventory.item_id
            == item_id).first()
    if warehouseitem:
        quantity = warehouseitem.quantity + 1
        warehouseitem.quantity = quantity
    else:
        warehouseitem = \
            models.Warehouse_Inventory(warehouse_id=itemforsale.warehouse,
                item_id=item_id, quantity=1, price=itemforsale.price)
        db.session.add(warehouseitem)
    db.session.commit()
    flash('Item successfully removed from your inventory!')
    return redirect(url_for('seller_items',
                    seller_id=session.get('seller_id')))


@app.route('/removeFromCart')
def removeFromCart():
    item_id = int(request.args.get('item_id'))
    seller_id = int(request.args.get('seller_id'))
    cart_item = \
        db.session.query(models.Cart_Item).filter(models.Cart_Item.cart_id
            == session.get('cart_id'), models.Cart_Item.item_id
            == item_id, models.Cart_Item.seller_id == seller_id).first()
    db.session.delete(cart_item)
    db.session.commit()
    flash('Item successfully removed from cart!')

    return redirect(url_for('cart'))


@app.route('/checkout', methods=['GET', 'POST'])
def checkout():
    cart_items = \
        db.session.query(models.Cart_Item).filter(models.Cart_Item.cart_id
            == session.get('cart_id')).all()
    if not cart_items:
        flash('cart is empty')
        return redirect(url_for('cart'))
    total = 0
    seller_total = 0
    order_max = \
        db.session.query(db.func.max(models.Order.order_id)).scalar()
    new_order = models.Order(order_id=order_max + 1,
                             buyer_id=session.get('buyer_id'),
                             order_date=datetime.today().strftime('%Y-%m-%d'
                             ), status='Pending')
    db.session.add(new_order)
    for cart_item in cart_items:
        item = \
            db.session.query(models.ItemForSale).filter(models.ItemForSale.item_id
                == cart_item.item_id, models.ItemForSale.seller_id
                == cart_item.seller_id).first()
        if cart_item.quantity > item.quantity:
            db.session.rollback()
            flash('insufficient number of copies of an item, please buy from other sellers'
                  )
            return redirect(url_for('cart'))

        if item.seller.user_id == session.get('user_id'):
            seller_total += cart_item.price * cart_item.quantity
        else:
            item.seller.profile.balance += cart_item.price \
                * cart_item.quantity

        item.quantity -= -cart_item.quantity
        if item.quantity == 0:
            db.session.delete(item)

        total += cart_item.price * cart_item.quantity

        new_order_item = models.Order_item(order_id=new_order.order_id,
                item_id=cart_item.item_id,
                seller_id=cart_item.seller_id,
                quantity=cart_item.quantity, price_each=cart_item.price)
        db.session.add(new_order_item)
        db.session.delete(cart_item)

    profile = \
        db.session.query(models.Profile).filter(models.Profile.user_id
            == session.get('user_id')).first()
    if total > profile.balance:
        db.session.rollback()
        flash('insufficient funds')
        return redirect(url_for('cart'))

    new_order.total = total
    profile.balance = profile.balance - total + seller_total
    db.session.commit()
    flash('Successfully checked out')
    return redirect(url_for('order_history'))


@app.route('/orders')
def order_history():
    user = \
        db.session.query(models.Profile).filter(models.Profile.user_id
            == session['user_id']).first()
    orders = \
        db.session.query(models.Order).filter(models.Order.buyer_id
            == session.get('buyer_id'
            )).order_by(models.Order.order_date.desc())
    if 'seller_id' in session.keys():
        seller_id = session['seller_id']
    else:
        seller_id = None

    return render_template('order_history.html', orders=orders,
                           profile=user, seller_id=seller_id)


@app.route('/sellers/<seller_id>')
def seller_items(seller_id):
    createseller = \
        db.session.query(models.Seller).filter(models.Seller.seller_id
            == seller_id).all()
    if createseller:
        results = \
            db.session.query(models.ItemForSale).filter(models.ItemForSale.seller_id
                == seller_id).all()
    else:
        new_seller = models.Seller(seller_id=seller_id,
                                   user_id=seller_id)
        db.session.add(new_seller)
        results = ''
        db.session.commit()
    user = \
        db.session.query(models.Profile).filter(models.Profile.user_id
            == session['user_id']).first()
    return render_template('seller_profile.html', seller_id=seller_id,
                           data=results, profile=user)


@app.route('/addBalance', methods=['GET', 'POST'])
def addBalance():
    user = \
        db.session.query(models.Profile).filter(models.Profile.user_id
            == session['user_id']).first()

    profile = \
        db.session.query(models.Profile).filter(models.Profile.user_id
            == session.get('user_id')).first()
    current_balance = profile.balance
    form = forms.AddBalance.form()
    if 'seller_id' in session.keys():
        seller_id = session['seller_id']
    else:
        seller_id = None
    if form.validate_on_submit():
        if form.amount.data < 0:
            flash('negative number, try again')
            return redirect(url_for('addBalance'))
        profile.balance += form.amount.data
        db.session.commit()
        flash('Balance added successfully!')
        return render_template('profile.html', profile=profile)
    if not len(form.errors) == 0:
        flash('try again')
    return render_template('add_balance.html', form=form,
                           current_balance=current_balance,
                           seller_id=seller_id, profile=user)


@app.route('/warehouses', methods=['GET', 'POST'])
def showWarehouses():
    warehouseQuery = db.session.query(models.Warehouse).all()
    warehouses = []
    warehouseNames = []
    for warehouse in warehouseQuery:
        warehouses.append(warehouse.warehouse_id)
        warehouseNames.append(warehouse.name)

    # itemNames = []
    # for itemId in warehouses:
    # ....query = db.session.query(models.Item).filter_by(item_id=itemId).one()
    # ....itemNames.append(query.item_name)

    return render_template('warehouse_list.html',
                           warehouseNames=warehouseNames,
                           warehouses=warehouses, len=len(warehouses))


@app.route('/warehouse', methods=['GET', 'POST'])
def getInventory():
    id = int(request.args.get('id'))
    items = \
        db.session.query(models.Warehouse_Inventory).filter_by(warehouse_id=id).all()
    itemIds = []
    quantities = []
    nameQuery = \
        db.session.query(models.Warehouse).filter_by(warehouse_id=id).first()

# ....prices = []

    name = nameQuery.name
    for item in items:
        itemIds.append(item.item_id)
        quantities.append(item.quantity)

# ........prices.append(item.price)

    itemNames = []
    for itemId in itemIds:
        query = \
            db.session.query(models.Item).filter_by(item_id=itemId).one()
        itemNames.append(query.item_name)
    return render_template(
        'warehouse.html',
        warehouse=id,
        itemIds=itemIds,
        items=itemNames,
        len=len(itemNames),
        quantities=quantities,
        name=name,
        )


@app.route('/removeFromWarehouse')
def removeFromWarehouse():
    item_id = int(request.args.get('item_id'))
    warehouse_id = int(request.args.get('warehouse_id'))
    warehouse_item = \
        db.session.query(models.Warehouse_Inventory).filter(models.Warehouse_Inventory.item_id
            == item_id, models.Warehouse_Inventory.item_id
            == item_id).first()
    if warehouse_item.quantity > 1:
        quantity = warehouse_item.quantity - 1
        warehouse_item.quantity = quantity
    else:
        db.session.delete(warehouse_item)
    itemforsale = \
        db.session.query(models.ItemForSale).filter(models.ItemForSale.item_id
            == item_id).first()
    if itemforsale:
        quantity = itemforsale.quantity + 1
        itemforsale.quantity = quantity
    else:
        itemforsale = \
            models.ItemForSale(seller_id=session.get('seller_id'),
                               item_id=item_id, quantity=1,
                               price=warehouse_item.price,
                               warehouse=warehouse_id)
        db.session.add(itemforsale)
    db.session.commit()
    flash('Item successfully added to your inventory!')
    return redirect(url_for('seller_items',
                    seller_id=session.get('seller_id')))


@app.route('/search_results/<results>', methods=['GET', 'POST'])
def search_results(results):
    if 'seller_id' in session.keys():
        seller_id = session['seller_id']
    else:
        seller_id = None
    user = \
        db.session.query(models.Profile).filter(models.Profile.user_id
            == session['user_id']).first()

    return render_template('search_results.html', results=results,
                           seller_id=seller_id, profile=user)


@app.route('/browse')
def browse():
    items = db.session.query(models.ItemForSale)
    return render_template('browse.html', items=items,
                           cart=session['cart_id'])


@app.route('/reviews_for_item/<int:product_id>', methods=['GET'])
def getAllReviews(product_id):
    reviews = \
        db.session.query(models.Review_Item).filter(models.Review_Item.item_id
            == product_id)
    user = \
        db.session.query(models.Profile).filter(models.Profile.user_id
            == session['user_id']).first()
    if 'seller_id' in session.keys():
        seller_id = session['seller_id']
    else:
        seller_id = None
    return render_template('reviews_for_item.html', Reviews=reviews,
                           profile=user, seller_id=seller_id)


@app.route('/create_item_review/<int:product_id>', methods=['GET',
           'POST'])
def addReview(product_id):
    form = forms.ReviewItemForm.form()
    user = \
        db.session.query(models.Profile).filter(models.Profile.user_id
            == session['user_id']).first()

    if form.validate_on_submit():
        if form.rating.data < 0:
            flash('negative rating, try again')
            return redirect(url_for('addReview', product_id=product_id))
        b_id = session.get('buyer_id')
        newReview = models.Review_Item(buyer_id=b_id,
                item_id=product_id,
                date=datetime.today().strftime('%Y-%m-%d'),
                review_text=form.review.data, rating=form.rating.data)
        print ()
        db.session.add(newReview)
        db.session.commit()
        currItem = \
            db.session.query(models.Item).filter(models.Item.item_id
                == product_id).first()
        newAv = \
            db.session.query(db.func.avg(models.Review_Item.rating)).filter(models.Review_Item.item_id
                == product_id)
        currItem.avg_rating = newAv
        db.session.commit()
        flash('Review added successfully!')
        return redirect(url_for('getAllReviews', product_id=product_id))
    if not len(form.errors) == 0:
        flash('Try again')

    if 'seller_id' in session.keys():
        seller_id = session['seller_id']
    else:
        seller_id = None
    return render_template('create_review_item.html', form=form,
                           profile=user, seller_id=seller_id)


@app.route('/reviews_for_seller/<int:seller_id>', methods=['GET'])
def getAllReviewsForSellers(seller_id):
    reviews = \
        db.session.query(models.Review_Seller).filter(models.Review_Seller.seller_id
            == seller_id)
    user = \
        db.session.query(models.Profile).filter(models.Profile.user_id
            == session['user_id']).first()
    if 'seller_id' in session.keys():
        seller_id = session['seller_id']
    else:
        seller_id = None
    return render_template('reviews_for_seller.html', Reviews=reviews,
                           profile=user, seller_id=seller_id)


@app.route('/create_seller_review/<int:seller_id>', methods=['GET',
           'POST'])
def addSellerReview(seller_id):
    form = forms.ReviewSellerForm.form()
    user = \
        db.session.query(models.Profile).filter(models.Profile.user_id
            == session['user_id']).first()

    if form.validate_on_submit():
        if form.rating.data < 0:
            flash('negative rating, try again')
            return redirect(url_for('addSellerReview',
                            seller_id=seller_id))
        b_id = session.get('buyer_id')
        newReview = models.Review_Seller(buyer_id=b_id,
                seller_id=seller_id,
                date=datetime.today().strftime('%Y-%m-%d'),
                review_text=form.review.data, rating=form.rating.data)
        print ()
        db.session.add(newReview)
        db.session.commit()
        flash('Review added successfully!')
        return redirect(url_for('getAllReviewsForSellers',
                 seller_id=seller_id))
    if not len(form.errors) == 0:
        flash('Try again')

    if 'seller_id' in session.keys():
        seller_id = session['seller_id']
    else:
        seller_id = None
    return render_template('create_review_seller.html', form=form,
                           profile=user, seller_id=seller_id)


@app.route('/item/<int:itemid>', methods=['GET', 'POST'])
def itemPage(itemid):
    item = db.session.query(models.Item).filter(models.Item.item_id
            == itemid).first()
    user = \
        db.session.query(models.Profile).filter(models.Profile.user_id
            == session['user_id']).first()
    itemsListed = \
        db.session.query(models.ItemForSale).filter(models.ItemForSale.item_id
            == itemid)
    count =  itemsListed.count()
    reviews = \
        db.session.query(models.Review_Item).filter(models.Review_Item.item_id
            == itemid)
    cart = session['cart_id']
    if 'seller_id' in session.keys():
        seller_id = session['seller_id']
    else:
        seller_id = None
    return render_template(
        'item.html',
        item=item,
        profile=user,
        items=itemsListed,
        cart=cart,
	count = count,
        reviews=reviews,
        seller_id=seller_id,
        )


@app.route('/profile', methods=['GET', 'POST'])
def profile():
    user = \
        db.session.query(models.Profile).filter(models.Profile.user_id
            == session['user_id']).first()
    if 'seller_id' in session.keys():
        seller_id = session['seller_id']
    else:
        seller_id = None
    return render_template('profile.html', profile=user,
                           seller_id=seller_id)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)

