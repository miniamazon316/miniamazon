from flask_wtf import FlaskForm
from wtforms import PasswordField, DecimalField, PasswordField, StringField, BooleanField, IntegerField, FloatField
from wtforms import SelectField, SubmitField, validators
from wtforms.validators import DataRequired, NumberRange, Email


class SignIn:
	@staticmethod
	def form():
		class F(FlaskForm):
			username = StringField('Username',  [DataRequired()])
			password = PasswordField('Password', [DataRequired()])
			submit = SubmitField('Submit')
		return F()

class Register:
	@staticmethod
	def form():
		class F(FlaskForm):
			name = StringField('Name',  [DataRequired()])
			email = StringField('Email Address', [DataRequired(), validators.Email()])
			phone = StringField('Phone Number',  [DataRequired()])
			address = StringField('Address',  [DataRequired()])
			username = StringField('Username',  [DataRequired()])
			password = PasswordField('Password', [DataRequired()])
			seller = BooleanField('Do you want to be a seller?')
			confirm_password = PasswordField('Password', [DataRequired(), validators.EqualTo('password')])
			submit = SubmitField('Submit')
		return F()
class Seller:
	@staticmethod
	def form(seller_ids):
		class F(FlaskForm):
			seller_sel = SelectField('Sellers',choices=[sn for sn in seller_ids])
			submit = SubmitField("Submit")
		return F()

class AddBalance:
	@staticmethod
	def form():
		class F(FlaskForm):
			amount = FloatField('Amount', [DataRequired()])
			submit = SubmitField('Submit')

		return F()
class Search:
	@staticmethod
	def form():
		class F(FlaskForm):
			search = StringField('Search Item')
			submit = SubmitField('Submit')

		return F()
class ReviewItemForm:
    @staticmethod
    def form():
        class F(FlaskForm):
            rating = DecimalField(
                'Provide a rating for this product', [DataRequired(), NumberRange(min = 1, max = 5, message = "Please enter a decimal from 1 to 5")])
            review = StringField('Review this Product',  [DataRequired()])
            submit = SubmitField('Submit')
        return F()
class ReviewSellerForm:
    @staticmethod
    def form():
        class F(FlaskForm):
            rating = DecimalField(
                'Provide a rating for this seller', [DataRequired(),NumberRange(min = 1, max = 5, message = "Please enter a decimal from 1 to 5")])
            review = StringField('Review this Seller',  [DataRequired()])
            submit = SubmitField('Submit')
        return F()

class NewPassword:
	@staticmethod
	def form():
		class F(FlaskForm):
			username = StringField('Username',  [DataRequired()])
			new_password = PasswordField('Password', [DataRequired()])
			confirm_new_password = PasswordField('Password', [DataRequired(), validators.EqualTo('new_password')])
			submit = SubmitField('Submit')
		return F()
