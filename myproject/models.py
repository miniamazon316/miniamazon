# from sqlalchemy import sql, orm
# from app import db
from flask_sqlalchemy import SQLAlchemy

#db = SQLAlchemy(app, session_options={'autocommit': False})
from db import db

class Profile(db.Model):
    __tablename__ = 'profile'
    user_id = db.Column('user_id', db.Integer, primary_key=True)
    email = db.Column('email', db.String(200), unique=True, nullable=False)
    # cart_id = db.Column('cartid', db.Integer, unique = True, nullable = False)
    phone = db.Column('phone', db.String(20), nullable=False)
    name = db.Column('name', db.String(200), nullable=False)
    balance = db.Column('balance', db.Float)
    address = db.Column('address', db.String(500), nullable=False)
    person = db.relationship('Person', backref='profile', uselist=False, lazy=True)
    buyer = db.relationship('Buyer', backref='profile', uselist=False, lazy=True)
    seller = db.relationship('Seller', backref='profile', uselist=False, lazy=True)


class Person(db.Model):
    __tablename__ = 'person'
    user_id = db.Column('user_id', db.Integer, db.ForeignKey('profile.user_id'), primary_key=True)
    username = db.Column('username', db.String(20))
    password = db.Column('password', db.String(20))
    # profile = db.relationship('Profile', backref='person', uselist=False)


class Buyer(db.Model):
    __tablename__ = 'buyer'
    buyer_id = db.Column('buyer_id', db.Integer, primary_key=True)
    user_id = db.Column('user_id', db.Integer, db.ForeignKey('profile.user_id'))
    orders = db.relationship('Order', backref='buyer')
    review_item = db.relationship('Review_Item', backref='buyer', uselist=False)
    review_seller = db.relationship('Review_Seller', backref='buyer', uselist=False)
    cart = db.relationship('Cart', backref='buyer', uselist=False)
    # profile = db.relationship('Profile', backref='Buyer', uselist=False)


class Seller(db.Model):
    __tablename__ = 'seller'
    seller_id = db.Column('seller_id', db.Integer, primary_key=True)
    user_id = db.Column('user_id', db.Integer, db.ForeignKey('profile.user_id'))
    items_for_sale = db.relationship('ItemForSale', backref='seller', lazy=True)
    Seller_history = db.relationship('Order_item')
    seller_reviews = db.relationship('Review_Seller')


class Item(db.Model):
    __tablename__ = 'item'
    item_id = db.Column('item_id', db.Integer, primary_key=True)
    item_name = db.Column('item_name', db.String(120))
    category_name = db.Column('category_name', db.String(200), db.ForeignKey('category.category_name'))
    description = db.Column(db.String(100000000000000))
    avg_rating = db.Column('avg_rating', db.Float)
    item_link = db.Column('item_link', db.String(120))
    listings = db.relationship('ItemForSale', backref='item')
    item_reviews = db.relationship('Review_Item', backref='item')
    order_items = db.relationship('Order_item', backref='item')
    cart_items = db.relationship('Cart_Item', backref='item')


class ItemForSale(db.Model):
    __tablename__ = 'item_for_sale'
    item_id = db.Column('item_id', db.Integer, db.ForeignKey('item.item_id'), primary_key=True)
    seller_id = db.Column('seller_id', db.Integer, db.ForeignKey('seller.seller_id'), primary_key=True)
    price = db.Column('price', db.Float)
    quantity = db.Column('quantity', db.Integer)
    warehouse = db.Column('warehouse', db.Integer)


class Category(db.Model):
    __tablename__ = 'category'
    category_name = db.Column(db.String(200), primary_key=True)
    items = db.relationship('Item', backref='category')


class Cart(db.Model):
    __tablename__ = 'cart'
    cart_id = db.Column(db.Integer, primary_key=True)
    buyer_id = db.Column(db.Integer, db.ForeignKey('buyer.buyer_id'), unique=True)
    items = db.relationship('Cart_Item', backref='cart')


class Cart_Item(db.Model):
    __tablename__ = 'cart_item'
    cart_id = db.Column('cart_id', db.Integer, db.ForeignKey('cart.cart_id'), primary_key=True)
    item_id = db.Column(db.Integer, db.ForeignKey('item.item_id'), primary_key=True)
    seller_id = db.Column(db.Integer, db.ForeignKey('seller.seller_id'), primary_key=True)
    price = db.Column('price', db.Float)
    quantity = db.Column('quantity', db.Integer)


class Order(db.Model):
    __tablename__ = 'order'
    order_id = db.Column(db.Integer, primary_key=True)
    buyer_id = db.Column(db.Integer, db.ForeignKey('buyer.buyer_id'))
    order_date = db.Column(db.DateTime)
    shipped_date = db.Column(db.DateTime)
    status = db.Column(db.String(200))
    total = db.Column(db.Float)
    items = db.relationship('Order_item', backref='order')


class Order_item(db.Model):
    __tablename__ = 'order_item'
    order_id = db.Column('order_id', db.Integer, db.ForeignKey('order.order_id'), primary_key=True)
    item_id = db.Column('item_id', db.Integer, db.ForeignKey('item.item_id'), primary_key=True)
    seller_id = db.Column('seller_id', db.Integer, db.ForeignKey('seller.seller_id'), primary_key=True)
    quantity = db.Column('quantity', db.Integer)
    price_each = db.Column('price_each', db.Float)


class Seller_history(db.Model):
    __tablename__ = 'seller_history'
    order_id = db.Column(db.Integer, db.ForeignKey('order.order_id'), primary_key=True)
    item_id = db.Column(db.Integer, db.ForeignKey('item_for_sale.item_id'), primary_key=True)
    seller_id = db.Column(db.Integer, db.ForeignKey('item_for_sale.seller_id'), primary_key=True)
    quantity = db.Column('quantity', db.Integer)
    date_purchased = db.Column('date_purchased', db.DateTime)
    price = db.Column('price', db.Float)


class Review_Seller(db.Model):
    __tablename__ = 'review_seller'
    buyer_id = db.Column(db.Integer, db.ForeignKey('buyer.buyer_id'), primary_key=True)
    seller_id = db.Column(db.Integer, db.ForeignKey('seller.seller_id'), primary_key=True)
    date = db.Column('date', db.DateTime, primary_key=True)
    review_text = db.Column('review_text', db.String(1000000))
    rating = db.Column('rating', db.Float)


class Review_Item(db.Model):
    __tablename__ = 'review_item'
    buyer_id = db.Column(db.Integer, db.ForeignKey('buyer.buyer_id'), primary_key=True)
    item_id = db.Column(db.Integer, db.ForeignKey('item.item_id'), primary_key=True)
    date = db.Column('date', db.DateTime, primary_key=True)
    review_text = db.Column('review_text', db.String(1000000))
    rating = db.Column('rating', db.Float)


class Warehouse(db.Model):
    __tablename__ = 'warehouse'
    warehouse_id = db.Column('warehouse_id', db.Integer, primary_key=True)
    name = db.Column('name', db.String(20))
    address = address = db.Column('address', db.String(500))
    inventory = db.relationship('Warehouse_Inventory', backref='Warehouse')
    shipments = db.relationship('Shipments')


class Warehouse_Inventory(db.Model):
    __tablename__ = 'warehouse_inventory'
    warehouse_id = db.Column(db.Integer, db.ForeignKey('warehouse.warehouse_id'), primary_key=True)
    item_id = db.Column(db.Integer, db.ForeignKey('item.item_id'), primary_key=True)
    quantity = db.Column(db.Integer)
    price = db.Column('price', db.Float)


class Shipments(db.Model):
    __tablename__ = 'shipments'
    warehouse_id = db.Column(db.Integer, db.ForeignKey('warehouse.warehouse_id'), primary_key=True)
    order_id = db.Column(db.Integer, db.ForeignKey('order.order_id'), primary_key=True)
    item_id = db.Column(db.Integer, db.ForeignKey('item.item_id'), primary_key=True)
